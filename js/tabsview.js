(function( global ) {
	'use strict'

	/**
	 * TabsView class.
	 *
	 * @constructor
	 * @param {Object} options - TabsView options.
	 * @param {String|Null} options.activeTab - unique name of tab that need to be set as active or return unique name of active tab. [Default value: ''].
	 * @param {String} options.renderTo - selector of container that will hold tabs structure. [Default value: 'body'].
	 * @param {Object} options.events - list events for tabs.  [Default value: '{}'] 
	 * @param {Array}  options.tabs - Preloading tabs. [Default value: '[]']         
	 */
	var TabsView = function( options ) {
		this.currentTab = options.activeTab || '';
		this.element = null;
		this.tabs = {};
		this.tabsIdList = [];
		this.events = options.events || {};
		this.renderTo = options.renderTo || 'body';

		// Initialization TabView
		this.init( options.tabs || [] );
	};

	/**
	 * Link to constructor's TabsView
	 */
	TabsView.prototype.constructor = TabsView;

	/**
	 * Initialization TabView. 
	 */
	TabsView.prototype.init = function( preloadTabs ) {
		var wrapper = document.createElement( 'div' ),
			tabset = document.createElement( 'ul' ),
			contentHolder = document.createElement( 'div' ),
			selector = document.querySelector( this.renderTo );

		wrapper.className = 'tabs-wrapper';
		tabset.className = 'tab-set';
		contentHolder.className = 'tabview-panel';	

		wrapper.appendChild( tabset );
		wrapper.appendChild( contentHolder );
		selector && selector.appendChild( wrapper );

		this.element = wrapper;
		this.element.tabset = tabset;
		this.element.contentHolder = contentHolder;

		// Add preload tabs
		if( preloadTabs.length ) {

			var addPreloadedTabs = function( tab ) {
				this.addTab( tab.name, tab.title, tab.content );
			}.bind( this );

			preloadTabs.forEach( addPreloadedTabs );
		}

	};

	/**
	 * Returns active tab or makes active tab for name.
	 */
	TabsView.prototype.activeTab = function() {

		if( arguments.length ) {
			var newActiveTab = arguments[0],
				tabList = this.tabs;
			
			if ( !tabList[ newActiveTab ] || tabList[ newActiveTab ].isActive() && newActiveTab === this.currentTab ) {
				return;
			}

			tabList[ this.currentTab ] && tabList[ this.currentTab ].hide();

			tabList[ newActiveTab ].show();
			this.currentTab = newActiveTab;

			return this;

		} else {
			return this.currentTab;
		}

	};

	/**
	 * Add new tab
	 */
	TabsView.prototype.addTab = function( tabName, tabTitle, tabContent ) {
		var self = this,
			tab = null; 

		tab = new TabsView.Tab({ 
			name: tabName, 
			title: tabTitle, 
			content: tabContent
		});

		// Action on tab switcher click
		tab.addEventListener('click', function( tabName ) {
			self.activeTab( tabName );
		});

		// Create preload events on tab
		for( var event in this.events ) {
			tab.addEventListener( event, self.events[ event ]);
		}
		

		if ( this.tabsIdList.indexOf( tab.id ) === -1 ) {
			this.tabs[ tab.id ] = tab;
			this.tabsIdList.push( tab.id );

			this.render();
		}
		

		if ( this.currentTab === tab.id ) {
			this.activeTab( tab.id );
		}

		return this;
	};

	/**
	 * Removes the tab.
	 */
	TabsView.prototype.removeTab = function( tabName ) {
		var removingTab = tabName || this.currentTab,
			indexTab = this.tabsIdList.indexOf( removingTab );

		if ( removingTab === this.currentTab ) {
			
			var next = this.tabsIdList[ indexTab + 1 ] || this.tabsIdList[ indexTab - 1 ];

			// Move active class to another tab
			if ( next ) {
				this.activeTab( next );
			}

		}

		this.tabsIdList.splice( indexTab, 1 );
		this.tabs[ removingTab ].destroy();
		delete this.tabs[ removingTab ];

		return this;
	};

	/**
	 * If arguments is not specified, this method returns the title of the tab
	 * otherwise, set the title to the tab
	 */
	TabsView.prototype.tabTitle = function( tabName, tabTitle ) {
		var id = tabName || this.currentTab;

		if( !tabTitle ) {
			return this.tabs[ id ].title;
		}

		this.tabs[ id ].setTitle( tabTitle );

		return this;
	};

	/**
	 * Render last tab
	 */
	TabsView.prototype.render = function() {
		var lastTab = this.tabsIdList[ this.tabsIdList.length - 1 ];
			this.element.tabset.appendChild( this.tabs[ lastTab ].element[0] );
			this.element.contentHolder.appendChild( this.tabs[ lastTab ].element[1] );
	};

	/**
	 * Tab class.
	 *
	 * @constructor
	 * @param {Object} definition - tab options.
	 * @param {String} [definition.name] - unique id of tab. [Default value: 'NewTab']
	 * @param {String} [definition.title] - text of tab switcher button. If it doesn't specified it would be taken from name of tab.
	 * @param {String} [definition.content] - string that contains HTML that need to be added or DOM node. [Default value: ''].
	 */
	TabsView.Tab = function( definition ) {
		this.id = definition.name || 'NewTab';
		this.element = [];
		this.title = definition.title || this.id;
		this.content = definition.content || '';

		this.init();
	};

	/**
	 * Link to constructor's Tab
	 */
	TabsView.Tab.prototype.constructor = TabsView.Tab;

	/**
	 * Initialization Tab
	 */
	TabsView.Tab.prototype.init = function() {

		this.element = [ createTab( this.title ), createTabContent( this.content ) ];

		// Create DOM elements needed for tabContent
		function createTabContent( tabContent ) {
			var content = document.createElement( 'div' );
			
			content.className = 'tab-panel';
			content.insertAdjacentHTML('beforeend', tabContent);

			return content;
		};

		// Create DOM elements needed for tab
		function createTab( tabTitle ) {
			var tab = document.createElement( 'li' ),
				tabLink = document.createElement( 'a' );

			tabLink.setAttribute( 'href', '#' );
			tab.className = 'tab-item';

			tabLink.appendChild( document.createTextNode( tabTitle ) );
			tab.appendChild( tabLink );

			return tab;
		};

		this.hide();
	};
	
	/**
	 * Shows tab and tab panel.
	 */
	TabsView.Tab.prototype.show = function() {
		addClass( this.element[0], 'active-tab' );
		addClass( this.element[1], 'active-panel' );
	};

	/**
	 * Hides tab and tab panel.
	 */
	TabsView.Tab.prototype.hide = function() {
		removeClass( this.element[0], 'active-tab' );
		removeClass( this.element[1], 'active-panel' );
	};

	/**
	 * Indicates whether tab is active or no.
	 *
	 * @returns {Boolean} state result.
	 */
	TabsView.Tab.prototype.isActive = function() {
		return hasClass( this.element[0], 'active-tab' );
	};

	/**
	 * Destroys the tab, tab's content and removes its nodes from DOM.
	 */
	TabsView.Tab.prototype.destroy = function() {
		var node = null;

		while(this.element.length) {
			node = this.element.shift();

			if ( node.parentNode ) {

				for( var event in this.handlers ) {

					this.handlers[ event ].forEach(function( handler ) {
						node.removeEventListener( event, handler, false);	
					});
					
				}

				node.parentNode.removeChild( node );
			} else{
				break;
			}
		}
	};

	/**
	 * Set new title of tab.
	 */
	TabsView.Tab.prototype.setTitle = function( title ) {

		if ( title === this.title ) {
			return;
		}

		this.title = title;
		this.element[0].childNodes[0].textContent = this.title;
	};

	/**
	 * Add events on Tab
	 */
	TabsView.Tab.prototype.addEventListener = function( event, callback ) {
		var self = this;

		var handler = function( event ) {
			callback.apply( self, [ self.id, this, event ]);
		};

		this.handlers = this.handlers || {};
		this.handlers[ event ] = this.handlers[ event ] || [];
		this.handlers[ event ].push( handler )

		this.element[0].addEventListener( event, handler, false);
	};


	/**
	 * Utils
	 */
	function addClass(el, cls) { 
		var c = el.className ? el.className.split(' ') : [];
		for (var i=0; i<c.length; i++) {
		if (c[i] == cls) return;
		}
		c.push(cls);
		el.className = c.join(' ');
	}

	function removeClass(el, cls) {
		var c = el.className.split(' ');
		for (var i=0; i<c.length; i++) {
			if (c[i] == cls) c.splice(i--, 1);
		}

		el.className = c.join(' ');
	}

	function hasClass(el, cls) {
		for (var c = el.className.split(' '),i=c.length-1; i>=0; i--) {
			if (c[i] == cls) return true;
		}
		return false;
	}

	return global.TabsView = TabsView;

})( this );